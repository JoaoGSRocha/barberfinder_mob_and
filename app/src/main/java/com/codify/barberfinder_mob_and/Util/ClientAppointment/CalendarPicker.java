package com.codify.barberfinder_mob_and.Util.ClientAppointment;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.TimePicker;
import com.codify.barberfinder_mob_and.R;
import java.util.Calendar;

public class CalendarPicker extends AppCompatActivity {

    private static final String TAG = "CalendarActivity";

    private CalendarView mCalendarView;
    private java.util.Calendar currentTime;
    private int hour, minute, year, month, dayOfMonth;
    private String format;
    private TextView tv;
    private long dm2s_val;

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public long getDm2s_val() {
        return dm2s_val;
    }

    public void setDm2s_val(long dm2s_val) {
        this.dm2s_val = dm2s_val;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        tv = (TextView) findViewById(R.id.timetxt);

        currentTime = Calendar.getInstance();

        hour = currentTime.get(Calendar.HOUR_OF_DAY);
        minute = currentTime.get(Calendar.MINUTE);



        selectedTimeFormat(hour);

        tv.setText(hour + " : "+ minute + " " + format);



        mCalendarView = (CalendarView) findViewById(R.id.calendarView);


        final TimePickerDialog timePickerDialog = new TimePickerDialog(
                CalendarPicker.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                selectedTimeFormat(hourOfDay);
                tv.setText(hourOfDay + " : " + minute + " " + format);
                currentTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                currentTime.set(Calendar.MINUTE, minute);
                setHour(currentTime.get(Calendar.HOUR_OF_DAY));
                setMinute(currentTime.get(Calendar.MINUTE));
            }
        }, hour, minute, true);


        timePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Intent intent = new Intent();
                intent.putExtra("Date",currentTime.getTimeInMillis());
                setResult(RESULT_OK, intent);
                finish();
            }
        });


        mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            long dm2s_val = 0L;
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int year, int month, int dayOfMonth) {
                String date = dayOfMonth + "/" + month + "/" + year;
                Log.d(TAG, "onSelectedDayChange: date: " + date);
                currentTime.set(Calendar.YEAR, year);
                currentTime.set(Calendar.MONTH, month);
                currentTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Intent intent = new Intent();
                timePickerDialog.show();
            }

        });
    }

    public long D2MS(int month, int day, int year, int hour, int minute){
       Calendar c = Calendar.getInstance();
       return c.getTimeInMillis();
    }

    public void selectedTimeFormat(int hour){
       if(hour == 0) {
           hour += 12;
           format = "AM";
       } else if (hour == 12) {
           format = "PM";
       } else if (hour > 12) {
           hour -= 12;
           format = "PM";
       } else {
           format = "AM";
       }
    }
}
