package com.codify.barberfinder_mob_and.Util.AppointmentQueue;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codify.barberfinder_mob_and.Entities.Appointment;
import com.codify.barberfinder_mob_and.Fragments.Client.Booking.SelectBarber;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AppointmentAdapter extends ArrayAdapter<Appointment> {

    Intent intent;
    Bundle bundle;
    int userid;
    String time;
    String barbername;
    String date;
    String shopname;
    TextView timetxt = new TextView(getContext());
    TextView barbernametxt = new TextView(getContext());
    TextView datetxt = new TextView(getContext());
    TextView shopnametxt = new TextView(getContext());

    private HashMap<Integer,String> barberName_hm = new HashMap<Integer, String>();
    private HashMap<Integer, String> shopName_hm = new HashMap<Integer, String>();
    private HashMap<Integer, Integer> shopSlot_hm = new HashMap<Integer, Integer>();

    public AppointmentAdapter(Context context, ArrayList<Appointment> appointments,
                              HashMap<Integer, String> shopName_hm,
                              HashMap<Integer, String> barberName_hm,
                              HashMap<Integer, Integer> shopSlot_hm,
                              int userid) {
        super(context, R.layout.row_appointment, appointments);
        this.userid = userid;
        setShopName_hm(shopName_hm);
        setBarberName_hm(barberName_hm);
        setShopSlot_hm(shopSlot_hm);
    }

    /*
        HashMaps Setters
     */
    public void setBarberName_hm(HashMap<Integer, String> barberName_hm) {
        this.barberName_hm = barberName_hm;
    }

    public void setShopName_hm(HashMap<Integer, String> shopName_hm) {
        this.shopName_hm = shopName_hm;
    }

    public void setShopSlot_hm(HashMap<Integer, Integer> shopSlot_hm) {
        this.shopSlot_hm = shopSlot_hm;
    }

    public String calcDaySlot(Integer dayIndex){
        switch (dayIndex){
            case 0: return "Monday";
            case 1: return "Tuesday";
            case 2: return "Wednesday";
            case 3: return "Thursday";
            case 4: return "Friday";
            case 5: return "Saturday";
            case 6: return "Sunday";
            default: return null;
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        intent = new Intent(parent.getContext(), SelectBarber.class);
        final Appointment appointment = getItem(position);

        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_appointment, parent, false);
        timetxt = (TextView) customView.findViewById(R.id.timetxt);
        barbernametxt = (TextView) customView.findViewById(R.id.barbernametxt);
        datetxt = (TextView) customView.findViewById(R.id.datetxt);
        shopnametxt = (TextView) customView.findViewById(R.id.shopnametxt);

        timetxt.setText(String.valueOf(appointment.getStarttime())); //TODO: Change into
        // correct format
        barbernametxt.setText(barberName_hm.get(appointment.getAppointmentid()));
        datetxt.setText(String.valueOf(appointment.getStarttime())); //TODO: Change into correct format
        shopnametxt.setText(shopName_hm.get(appointment.getAppointmentid()));

        return customView;
    }


}
