package com.codify.barberfinder_mob_and.Util;

import com.codify.barberfinder_mob_and.Entities.DaySlot;

// Java program implementing Singleton class
// with getInstance() method
class Singleton
{
    // static variable single_instance of type Singleton
    private static Singleton single_instance = null;

    public DaySlot getDaySlot() {
        return daySlot;
    }

    public void setDaySlot(DaySlot daySlot) {
        this.daySlot = daySlot;
    }

    // variable of type String
    public DaySlot daySlot;

    // private constructor restricted to this class itself
    private Singleton()
    {
        daySlot = new DaySlot();
    }

    // static method to create instance of Singleton class
    public static Singleton getInstance()
    {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }
}
