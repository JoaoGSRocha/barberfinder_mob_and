package com.codify.barberfinder_mob_and.Util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;


import com.codify.barberfinder_mob_and.Entities.Buyable;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;

//Can be refactored to be generic!
public class BuyableAdapter extends ArrayAdapter<Buyable> {

    public BuyableAdapter(Context context, ArrayList<Buyable> foods) {
        super(context, R.layout.row_buyable, foods);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // default -  return super.getView(position, convertView, parent);
        // add the layout
        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_buyable, parent, false);
        // get references.
        Buyable buy = getItem(position);
        TextView txtBuyable = (TextView) customView.findViewById(R.id.timetxt);
        TextView txtPrice = (TextView) customView.findViewById(R.id.prcvaluetxt);
        TextView txtDuration = (TextView) customView.findViewById(R.id.drtvaluetxt);

        Button btnEdit = (Button) customView.findViewById(R.id.btnSelectBarber);
        Button btnDelete = (Button) customView.findViewById(R.id.btnDeleteBarber);

        // dynamically update the text from the array
        txtBuyable.setText(String.valueOf(buy.getName()));
        txtPrice.setText(String.valueOf(buy.getPrice()));
        txtDuration.setText(String.valueOf(buy.getTime()));
        // using the same image every time
        // Now we can finally return our custom View or custom item
        return customView;
    }
}
