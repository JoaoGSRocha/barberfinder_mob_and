package com.codify.barberfinder_mob_and.Util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.codify.barberfinder_mob_and.Entities.Shop;
import com.codify.barberfinder_mob_and.Fragments.Client.Booking.Timetable.WeekDayPicker;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;

import static android.support.v4.content.ContextCompat.startActivity;


public class ShopAdapter extends ArrayAdapter<Shop> {

    private Context context;
    private Intent intent;
    private Bundle bundle;
    private Integer usrID;
    private long time;

    public ShopAdapter(Context context, ArrayList<Shop> shops, int usrID, long time) {
        super(context, R.layout.row_shop, shops);
        this.context = context;
        this.usrID = usrID;
        this.time = time;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_shop, parent, false);

        if(getItem(position) != null) {
            final Shop shop = getItem(position);
            TextView txtShop = (TextView) customView.findViewById(R.id.shopnametxt);
            Button btnView = (Button) customView.findViewById(R.id.btn_view);
            txtShop.setText(String.valueOf(shop.getName()));
            final Intent intent = new Intent(context, WeekDayPicker.class);
            bundle = new Bundle();

            btnView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle.putInt("SHOPID",Integer.valueOf(shop.getShopid()));
                    intent.putExtra("SHOPID", Integer.valueOf(shop.getShopid()));
                    intent.putExtra("USERID", usrID);
                    intent.putExtra("TIME", time);
                    Log.i("BUNDLE SHOPID", String.valueOf(bundle.getInt("SHOPID")));
                    startActivity(context, intent, bundle);
                }
            });
        }

        return customView;
    }
}
