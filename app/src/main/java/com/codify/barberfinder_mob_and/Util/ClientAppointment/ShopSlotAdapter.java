package com.codify.barberfinder_mob_and.Util.ClientAppointment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.codify.barberfinder_mob_and.Entities.ShopSlot;
import com.codify.barberfinder_mob_and.Fragments.Client.Booking.SelectBarber;
import com.codify.barberfinder_mob_and.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static android.support.v4.content.ContextCompat.startActivity;

//Can be refactored to be generic!
public class ShopSlotAdapter extends ArrayAdapter<ShopSlot> {

    TextView barbername;

    Intent intent;
    Bundle bundle;
    int userid;
    long time;

    public ShopSlotAdapter(Context context, ArrayList<ShopSlot> shopSlots, int userid, long time) {
        super(context, R.layout.row_shop_slot, shopSlots);
        this.userid = userid;
        this.time = time;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        intent = new Intent(parent.getContext(), SelectBarber.class);
        // default -  return super.getView(position, convertView, parent);
        // add the layout
        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_shop_slot, parent, false);
        // get references.
        if(getItem(position) != null) {
            final ShopSlot shopSlot = getItem(position);
            TextView slottime = (TextView) customView.findViewById(R.id.timetxt);
            TextView vacant = (TextView) customView.findViewById(R.id.barbernametxt);
            Button btnBooking = (Button) customView.findViewById(R.id.btn_Cancel);
            if(shopSlot.getAppointmentid() != null)
                btnBooking.setVisibility(View.INVISIBLE);
            vacant.setText((shopSlot.getAppointmentid() == null)
                        ? "Vacant" : "Booked");

            slottime.setText(shopSlot.getTimetxt());

            intent = new Intent(getContext(), SelectBarber.class);
            bundle = new Bundle();
            btnBooking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle.putInt("SHOPSLOTID", getItem(position).getShopsplotid());
                    bundle.putInt("SHOPID",Integer.valueOf(shopSlot.getShopid()));
                    intent.putExtra("SHOPID", Integer.valueOf(shopSlot.getShopid()));
                    intent.putExtra("SHOPSLOTID", Integer.valueOf(shopSlot.getShopsplotid()));
                    intent.putExtra("USERID", userid);

                    try {
                        bundle.putLong("TIME", Long.valueOf(parseDate(shopSlot.getTimetxt())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        intent.putExtra("TIME", Long.valueOf(parseDate(shopSlot.getTimetxt())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        Log.i("###TIMEOUT", String.valueOf(parseDate(shopSlot.getTimetxt())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    startActivity(getContext(), intent, bundle);
                }
            });
        }
        else {
            Log.e("NULL SHopSlot", "null shopslot");
        }

        return customView;
    }

    private static long parseDate(String text)
            throws ParseException
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.parse(text).getTime();
    }
}
