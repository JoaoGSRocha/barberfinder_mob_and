package com.codify.barberfinder_mob_and.Util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.Activities.Create.Shop.SelectShopBuyable;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;

import static android.support.v4.content.ContextCompat.startActivity;


public class BarberSelectAdapter extends ArrayAdapter<Barber> {

    TextView barbername;
    Intent intent;
    private  Context context;
    Bundle bundle;
    long timeSlot;
    int shopid;
    int shopslotid;
    int usrID;


    public BarberSelectAdapter(Context context, ArrayList<Barber> barbers, long timeSlot, int
            shopid, int shopslotid, int usrdID) {
        super(context, R.layout.row_barber_select, barbers);
        this.context = context;
        this.timeSlot = timeSlot;
        this.shopid = shopid;
        this.shopslotid = shopslotid;
        this.usrID = usrdID;

    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        // default -  return super.getView(position, convertView, parent);
        // add the layout
        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_barber_select, parent, false);
        // get references.
        final Barber barber = getItem(position);
        TextView txtBarber = (TextView) customView.findViewById(R.id.barberName);

        Button btnSelect = (Button) customView.findViewById(R.id.btnSelectBarber);


        intent = new Intent(getContext(), SelectShopBuyable.class);
        bundle = new Bundle();
        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, barber.getName(), Toast.LENGTH_SHORT).show();
                bundle.putInt("SHOPSLOTID", shopslotid);
                bundle.putInt("BARBERID",Integer.valueOf(barber.getBarberid()));
                intent.putExtra("BARBERID", Integer.valueOf(barber.getBarberid()));
                intent.putExtra("SHOPSLOTID", shopslotid);
                bundle.putLong("TIME", timeSlot);
                intent.putExtra("TIME", timeSlot);
                bundle.putInt("SHOPID", shopid);
                intent.putExtra("SHOPID", shopid);
                intent.putExtra("USERID", usrID);
                Log.i("###SELECTBARBER VALS", "shopid "+shopid+" time "+timeSlot+" barberid "+
                        barber.getBarberid());
                startActivity(parent.getContext(), intent, bundle);
            }
        });

        // dynamically update the text from the array
        txtBarber.setText(String.valueOf(barber.getName()));
        // using the same image every time
        // Now we can finally return our custom View or custom item
        return customView;
    }
}
