package com.codify.barberfinder_mob_and.Util;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.Activities.Create.Shop.CreateShopBarber;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;


public class BarberEditAdapter extends ArrayAdapter<Barber> {

    TextView barbername;
    Intent intent;
    private  Context context;

    public BarberEditAdapter(Context context, ArrayList<Barber> foods) {
        super(context, R.layout.row_barber_edit, foods);
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // default -  return super.getView(position, convertView, parent);
        // add the layout
        LayoutInflater myCustomInflater = LayoutInflater.from(getContext());
        View customView = myCustomInflater.inflate(R.layout.row_barber_edit, parent, false);
        // get references.
        final Barber barber = getItem(position);
        TextView txtBarber = (TextView) customView.findViewById(R.id.timetxt);

        Button btnEdit = (Button) customView.findViewById(R.id.btnSelectBarber);
        Button btnDelete = (Button) customView.findViewById(R.id.btnDeleteBarber);

        Intent intent = new Intent(context, CreateShopBarber.class);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, barber.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        // dynamically update the text from the array
        txtBarber.setText(String.valueOf(barber.getName()));
        // using the same image every time
        // Now we can finally return our custom View or custom item
        return customView;
    }
}
