package com.codify.barberfinder_mob_and.Util;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;

public class HttpOperations {

    public void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }

    public void inCaseOfErrorTry(Context context, Response<?> response)
            throws IOException, JSONException {
        JSONObject jObjError = new JSONObject(response.errorBody().string());
        Toast.makeText(context, jObjError
                .getString("message"), Toast.LENGTH_LONG).show();
    }

    public void inCaseOfErrorCatch(Context context, Exception e){
        Toast.makeText(context, e.getMessage(),
                Toast.LENGTH_LONG).show();

    }

}
