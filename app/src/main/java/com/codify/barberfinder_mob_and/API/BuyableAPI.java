package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.Buyable;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BuyableAPI {

    @GET("buyable/findall")
    Call<List<Buyable>> findAll();

    @GET("buyable/find/{id}")
    Call<Buyable> find(@Path("id") Integer id);

    @POST("buyable/create")
    Call<Buyable> create(@Body Buyable buyable);

    @PUT("buyable/update")
    Call<Void> update(@Body Buyable buyable);

    @DELETE("buyable/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


