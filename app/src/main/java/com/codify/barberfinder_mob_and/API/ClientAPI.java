package com.codify.barberfinder_mob_and.API;


import com.codify.barberfinder_mob_and.Entities.Client;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ClientAPI {

    @GET("client/findall")
    Call<List<Client>> findAll();

    @GET("client/find/{id}")
    Call<Client> find(@Path("id") Integer id);

    @POST("client/create")
    Call<Client> create(@Body Client client);

    @PUT("client/update")
    Call<Void> update(@Body Client client);

    @DELETE("client/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


