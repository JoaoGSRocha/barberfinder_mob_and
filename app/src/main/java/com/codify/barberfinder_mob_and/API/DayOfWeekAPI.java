package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.DayOfWeek;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DayOfWeekAPI {

    @GET("dayofweek/findall")
    Call<List<DayOfWeek>> findAll();

    @GET("dayofweek/find/{id}")
    Call<DayOfWeek> find(@Path("id") Integer id);

    @POST("dayofweek/create")
    Call<DayOfWeek> create(@Body DayOfWeek dayofweek);

    @PUT("dayofweek/update")
    Call<Void> update(@Body DayOfWeek dayofweek);

    @DELETE("dayofweek/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


