package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.ShopTimetable;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ShopTimetableAPI {

    @GET("shoptime/findall")
    Call<List<ShopTimetable>> findAll();

    @GET("shoptime/find/{id}")
    Call<ShopTimetable> find(@Path("id") Integer id);

    @POST("shoptime/create")
    Call<ShopTimetable> create(@Body ShopTimetable shopTime);

    @PUT("shoptime/update")
    Call<Void> update(@Body ShopTimetable shopTime);

    @DELETE("shoptime/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


