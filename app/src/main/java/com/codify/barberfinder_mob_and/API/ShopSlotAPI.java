package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.ShopSlot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ShopSlotAPI {

    @GET("shopslot/findall")
    Call<List<ShopSlot>> findAll();

    @GET("shopslot/find/{id}")
    Call<ShopSlot> find(@Path("id") Integer id);

    @POST("shopslot/create")
    Call<ShopSlot> create(@Body ShopSlot shopslot);

    @PUT("shopslot/update")
    Call<Void> update(@Body ShopSlot shopslot);

    @DELETE("shopslot/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


