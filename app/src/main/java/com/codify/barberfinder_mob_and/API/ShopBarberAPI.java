package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.ShopBarber;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ShopBarberAPI {

    @GET("shopbarber/findall")
    Call<List<ShopBarber>> findAll();

    @GET("shopbarber/find/{id}")
    Call<ShopBarber> find(@Path("id") Integer id);

    @POST("shopbarber/create")
    Call<ShopBarber> create(@Body ShopBarber shopbarber);

    @PUT("shopbarber/update")
    Call<Void> update(@Body ShopBarber shopbarber);

    @DELETE("shopbarber/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


