package com.codify.barberfinder_mob_and.API;


import com.codify.barberfinder_mob_and.Entities.Appointment;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AppointmentAPI {

    @GET("appointment/findall")
    Call<List<Appointment>> findAll();

    @GET("appointment/find/{id}")
    Call<Appointment> find(@Path("id") Integer id);

    @POST("appointment/create")
    Call<Appointment> create(@Body Appointment appointment);

    @PUT("appointment/update")
    Call<Void> update(@Body Appointment appointment);

    @DELETE("appointment/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


