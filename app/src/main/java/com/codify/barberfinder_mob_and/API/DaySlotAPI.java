package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.DaySlot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DaySlotAPI {

    @GET("dayslot/findall")
    Call<List<DaySlot>> findAll();

    @GET("dayslot/find/{id}")
    Call<DaySlot> find(@Path("id") Integer id);

    @POST("dayslot/create")
    Call<DaySlot> create(@Body DaySlot dayslot);

    @PUT("dayslot/update")
    Call<Void> update(@Body DaySlot dayslot);

    @DELETE("dayslot/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


