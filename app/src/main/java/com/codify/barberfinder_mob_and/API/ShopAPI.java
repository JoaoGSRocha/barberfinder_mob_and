package com.codify.barberfinder_mob_and.API;

import com.codify.barberfinder_mob_and.Entities.Shop;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ShopAPI {

    @GET("shop/findall")
    Call<List<Shop>> findAll();

    @GET("shop/find/{id}")
    Call<Shop> find(@Path("id") Integer id);

    @POST("shop/create")
    Call<Shop> create(@Body Shop shop);

    @PUT("shop/update")
    Call<Void> update(@Body Shop shop);

    @DELETE("shop/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


