package com.codify.barberfinder_mob_and.API;


import com.codify.barberfinder_mob_and.Entities.Barber;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BarberAPI {

    @GET("barber/findall")
    Call<List<Barber>> findAll();

    @GET("barber/find/{id}")
    Call<Barber> find(@Path("id") Integer id);

    @POST("barber/create")
    Call<Barber> create(@Body Barber barber);

    @PUT("barber/update")
    Call<Void> update(@Body Barber barber);

    @DELETE("barber/delete/{id}")
    Call<Void> delete(@Path("id") String id);
}


