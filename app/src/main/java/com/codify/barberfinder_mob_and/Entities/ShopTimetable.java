package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShopTimetable implements Serializable {

    @SerializedName("shoptimeid")
    private Integer shoptimeid;

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("openhour")
    private long openhour;

    @SerializedName("closinghour")
    private long closinghour;

    public Integer getShoptimeid() { return shoptimeid; }

    public void setShoptimeid(Integer shoptimeid) { this.shoptimeid = shoptimeid; }

    public Integer getShopid() { return shopid; }

    public void setShopid(Integer shopid) { this.shopid = shopid; }

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }

    public long getOpenhour() { return openhour; }

    public void setOpenhour(long openhour) { this.openhour = openhour; }

    public long getClosinghour() { return closinghour; }

    public void setClosinghour(long closinghour) { this.closinghour = closinghour; }
}
