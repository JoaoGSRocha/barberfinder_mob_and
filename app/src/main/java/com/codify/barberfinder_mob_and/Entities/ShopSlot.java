package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShopSlot implements Serializable {

    @SerializedName("shopslotid")
    private Integer shopsplotid;

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("slotid")
    private Integer slotid;

    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("appointmentid")
    private Integer appointmentid;

    public Integer getShopsplotid() {
        return shopsplotid;
    }

    private String timetxt;

    public void setShopsplotid(Integer shopsplotid) {
        this.shopsplotid = shopsplotid;
    }

    public Integer getShopid() {
        return shopid;
    }

    public void setShopid(Integer shopid) {
        this.shopid = shopid;
    }

    public Integer getSlotid() {
        return slotid;
    }

    public void setSlotid(Integer slotid) {
        this.slotid = slotid;
    }

    public Integer getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(Integer appointmentid) {
        this.appointmentid = appointmentid;
    }

    public String getTimetxt() { return timetxt; }

    public void setTimetxt(String timetxt) { this.timetxt = timetxt; }

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }
}
