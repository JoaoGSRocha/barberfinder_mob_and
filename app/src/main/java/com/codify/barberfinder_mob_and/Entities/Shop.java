package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Shop implements Serializable {

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("name")
    private String name;

    @SerializedName("locationid")
    private int locationid;

    @SerializedName("userid")
    private int userid;

    public Integer getShopid() {
        return shopid;
    }

    public void setShopid(Integer shopid) {
        this.shopid = shopid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public int getUserid() { return userid; }

    public void setUserid(int userid) { this.userid = userid; }
}