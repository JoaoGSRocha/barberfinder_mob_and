package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShopBarber implements Serializable {
    @SerializedName("shopbarberid")
    private int shopbarberid;

    @SerializedName("barberid")
    private int barberid;

    @SerializedName("shopid")
    private Integer shopid;



    public int getShopbarberid() {
        return shopbarberid;
    }

    public void setShopbarberid(int shopbarberid) {
        this.shopbarberid = shopbarberid;
    }

    public Integer getShopid() {
        return shopid;
    }

    public void setShopid(Integer shopid) {
        this.shopid = shopid;
    }

    public int getBarberid() {
        return barberid;
    }

    public void setBarberid(int barberid) {
        this.barberid = barberid;
    }


}
