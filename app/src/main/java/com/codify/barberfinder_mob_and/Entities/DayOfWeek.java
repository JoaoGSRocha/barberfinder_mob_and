package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

public class DayOfWeek {
    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("dayofweek")
    private String dayofweek;

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }

    public String getDayofweek() { return dayofweek; }

    public void setDayofweek(String dayofweek) { this.dayofweek = dayofweek; }
}
