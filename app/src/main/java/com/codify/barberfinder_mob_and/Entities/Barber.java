package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Barber implements Serializable {

    @SerializedName("barberid")
    private Integer barberid;

    @SerializedName("name")
    private String name;

    @SerializedName("alsohome")
    private int alsohome;

    @SerializedName("userid")
    private int userid;

    public Integer getBarberid() {
        return barberid;
    }

    public void setBarberid(Integer barberid) {
        this.barberid = barberid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlsohome() {
        return alsohome;
    }

    public void setAlsohome(int alsohome) {
        this.alsohome = alsohome;
    }

    public int getUserid() { return userid; }

    public void setUserid(int userid) { this.userid = userid; }
}