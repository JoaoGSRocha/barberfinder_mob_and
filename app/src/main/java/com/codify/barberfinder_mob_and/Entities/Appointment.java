package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Appointment implements Serializable {

    @SerializedName("appointmentid")
    private Integer appointmentid;

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("barberid")
    private Integer barberid;

    @SerializedName("clientid")
    private Integer clientid;

    @SerializedName("buyableid")
    private Integer buyableid;

    @SerializedName("starttime")
    private long starttime;

    public Integer getAppointmentid() { return appointmentid; }

    public void setAppointmentid(Integer appointmentid) { this.appointmentid = appointmentid; }

    public Integer getShopid() { return shopid; }

    public void setShopid(Integer shopid) { this.shopid = shopid; }

    public Integer getBarberid() { return barberid; }

    public void setBarberid(Integer barberid) { this.barberid = barberid; }

    public Integer getClientid() { return clientid; }

    public void setClientid(Integer clientid) { this.clientid = clientid; }

    public Integer getBuyableid() { return buyableid; }

    public void setBuyableid(Integer buyableid) { this.buyableid = buyableid; }

    public long getStarttime() { return starttime; }

    public void setStarttime(long starttime) { this.starttime = starttime; }
}
