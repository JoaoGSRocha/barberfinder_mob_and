package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Buyable implements Serializable {

    @SerializedName("buyableid")
    private Integer buyableid;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private float  price;

    @SerializedName("discount")
    private float discount;

    @SerializedName("time")
    private long time;

    public Integer getBuyableid() {
        return buyableid;
    }

    public void setBuyableid(Integer buyableid) {
        this.buyableid = buyableid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}