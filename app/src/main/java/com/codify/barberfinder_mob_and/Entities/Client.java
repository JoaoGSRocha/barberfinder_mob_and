package com.codify.barberfinder_mob_and.Entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Client implements Serializable {

    @SerializedName("clientid")
    private Integer clientid;

    @SerializedName("name")
    private String name;

    @SerializedName("mobile")
    private int mobile;

    @SerializedName("userid")
    private Integer userid;

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserid() { return userid; }

    public void setUserid(Integer userid) { this.userid = userid; }

    public int getMobile() { return mobile; }

    public void setMobile(int mobile) { this.mobile = mobile; }
}