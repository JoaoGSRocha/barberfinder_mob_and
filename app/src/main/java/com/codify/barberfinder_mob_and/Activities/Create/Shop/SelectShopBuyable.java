package com.codify.barberfinder_mob_and.Activities.Create.Shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.BuyableAPI;
import com.codify.barberfinder_mob_and.Util.BuyableAdapter;
import com.codify.barberfinder_mob_and.Entities.Buyable;
import com.codify.barberfinder_mob_and.Fragments.Client.Booking.CreateAppointment;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectShopBuyable extends AppCompatActivity {

    private static ArrayList<Buyable> buyables_al;
    private Intent intent;

    public static ArrayList<Buyable> getBuyables_al() {
        return buyables_al;
    }

    public void setBarbers_al(ArrayList<Buyable> buyables_al) {
        this.buyables_al = buyables_al;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_buyable);
        buyables_al = new ArrayList<Buyable>();

        loadData();
    }

    public void loadData() {
        try {
                BuyableAPI buyableAPI =
                        APIClient.getClient().create(BuyableAPI.class);
                buyableAPI.findAll().enqueue(new Callback<List<Buyable>>() {
                    @Override
                    public void onResponse(Call<List<Buyable>> call,
                                           Response<List<Buyable>> response) {
                        if(response.isSuccessful()) {

                            for(Buyable buyable : response.body()) {
                                int i=0;
                                buyables_al.add(buyable);
                                Log.i("###BuyableListSuccess"+i++, buyable.toString());
                            }
                        }
                        else{
                            int i=0;
                            for(Buyable buyable : response.body()){
                                Log.i("###BuyableListFailed"+i++, buyable.toString());
                            }
                        }
                        setBarbers_al(buyables_al);
                        ListView();

                    }

                @Override
                public void onFailure(Call<List<Buyable>> call,
                                      Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(SelectShopBuyable.this, t.getMessage(), Toast.LENGTH_SHORT);
                }
            });
        } catch (Exception e) {
            Toast.makeText(SelectShopBuyable.this, e.getMessage(), Toast.LENGTH_SHORT);
        }


    }

    private void nextActivity(Integer buyableid){
        long time = getIntent().getLongExtra("TIME", 0L);
        int shopid = getIntent().getIntExtra("SHOPID", 0);
        int barberid = getIntent().getIntExtra("BARBERID", 0);
        int shopslotid = getIntent().getIntExtra("SHOPSLOTID", 0);
        int usrID = getIntent().getIntExtra("USERID", 0);
        Log.i("###Acti VALS", "time " +time+" shopid "+shopid+" barberid "+barberid);
        if(getIntent().getLongExtra("TIME", 0L) != 0L) {
            intent = new Intent(SelectShopBuyable.this, CreateAppointment.class);
            intent.putExtra("TIME", time) ;
            intent.putExtra("SHOPID", shopid);
            intent.putExtra("BARBERID", barberid);
            intent.putExtra("BUYABLEID", buyableid);
            intent.putExtra("SHOPSLOTID", shopslotid);
            intent.putExtra("USERID", usrID);
            startActivity(intent);
        }
        else{
            Log.i("###IN DEV...", "in dev..other activities");
        }
    }

    private void ListView(){

        Log.i("Test",buyables_al.get(0).toString());
        int usrID = getIntent().getIntExtra("USERID", 0);
        ListAdapter buyableAdapter = new BuyableAdapter(this, buyables_al);
        ListView buyaListView = (ListView) findViewById(R.id.buyableListView);
        buyaListView.setAdapter(buyableAdapter);
        buyaListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Buyable buy = (Buyable) parent.getItemAtPosition(position);
                        Toast.makeText(SelectShopBuyable.this, buy.toString(), Toast.LENGTH_SHORT).show();
                        Log.i("###BUYID", String.valueOf(buy.getBuyableid()));
                        nextActivity(buy.getBuyableid());
                    }
                }
        );
    }
    
    
}
