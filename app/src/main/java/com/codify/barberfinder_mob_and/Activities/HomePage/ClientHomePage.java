package com.codify.barberfinder_mob_and.Activities.HomePage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codify.barberfinder_mob_and.R;

public class ClientHomePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_home_page);
    }
}
