package com.codify.barberfinder_mob_and.Activities.Create.Shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.BarberAPI;
import com.codify.barberfinder_mob_and.API.ShopBarberAPI;
import com.codify.barberfinder_mob_and.API.UserAPI;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.Entities.ShopBarber;
import com.codify.barberfinder_mob_and.Entities.User;
import com.codify.barberfinder_mob_and.R;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateShopBarber extends AppCompatActivity {
    EditText name;
    Switch home;
    Intent pIntent;
    int shopID;
    int barberID;
    int userID;
    boolean repeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_shopbarber);

        name = (EditText) findViewById(R.id.input_name);
        home = (Switch) findViewById(R.id.switch1);
        shopID = getIntent().getIntExtra("SHOPID", 0);
        userID = getIntent().getIntExtra("USERID", 0);
        Log.i("###BARBERCRT", String.valueOf(shopID));

        Button addMoreButton = (Button) findViewById(R.id.btn_addMore);
        Button nextButton = (Button)  findViewById(R.id.btn_next);
        addMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser_Callback();
                repeat = true;
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser_Callback();
                repeat = false;

            }
        });

    }

    protected void createBarber(Integer userID){


        httpOperations();
        Barber barber = setBarberFormValues(userID);
        BarberAPI barberAPI =
                APIClient.getClient().create(BarberAPI.class);

        barberAPI.create(barber).enqueue(new Callback<Barber>() {
            @Override
            public void onResponse(Call<Barber> call, Response<Barber> response) {
                if(response.isSuccessful()){
                    Log.i("###BARBERID", String.valueOf(response.body().getBarberid()));
                    createShopBarber_Callback(response.body().getBarberid());
                }
                else
                    Log.i("###BARBERRESPONSEERRRoR","error");
            }

            @Override
            public void onFailure(Call<Barber> call, Throwable t) {
                Toast.makeText(CreateShopBarber.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    protected ShopBarber createShopBarber(int barberID){
        if(shopID==0)
            return null;
        ShopBarber shopBarber = new ShopBarber();
        shopBarber.setBarberid(barberID);
        shopBarber.setShopid(shopID);
        return  shopBarber;
    }

    protected void createShopBarber_Callback(int barberID){
        final ShopBarber shopBarber = createShopBarber(barberID);
        if(null == shopBarber){
           redirectToNextActivity();
        }
        ShopBarberAPI shopBarberAPI =
                APIClient.getClient().create(ShopBarberAPI.class);

        shopBarberAPI.create(shopBarber).enqueue(new Callback<ShopBarber>() {
            @Override
            public void onResponse(Call<ShopBarber> call, Response<ShopBarber> response) {
                if(response.isSuccessful()){
                    int shopbarberID = response.body().getShopbarberid();
                    Log.i("###ShopBarberID", String.valueOf(shopbarberID));
                    redirectToNextActivity();
                }

                    Log.i("###SHOPBARBERRROR","error");
            }

            @Override
            public void onFailure(Call<ShopBarber> call, Throwable t) {
                Toast.makeText(CreateShopBarber.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void createUser_Callback(){
       final User user = setUserFormValues();
       if(null == user){
           redirectToNextActivity();
       }
        UserAPI userAPI =
                APIClient.getClient().create(UserAPI.class);

       userAPI.create(user).enqueue(new Callback<User>() {
           @Override
           public void onResponse(Call<User> call, Response<User> response) {
               if (response.isSuccessful()) {
                   createBarber(response.body().getUserid());
               }
           }

           @Override
           public void onFailure(Call<User> call, Throwable t) {
               Toast.makeText(CreateShopBarber.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
           }
       });
    }

    private void redirectToNextActivity() {
        if(shopID==0 && repeat)
            startActivity(new Intent(CreateShopBarber.this, CreateShopBarber.class)
                    .putExtra("SHOPID", shopID));
        else if(shopID==0 && !repeat)
            startActivity(new Intent(CreateShopBarber.this, CreateShopBarber.class)
                    .putExtra("SHOPID", shopID));
        else if(shopID!=0 && repeat)
            startActivity(new Intent(CreateShopBarber.this, CreateShopBarber.class)
                    .putExtra("SHOPID", shopID).putExtra("USERID", userID));
        else if(shopID!=0 && !repeat)
            startActivity(new Intent(CreateShopBarber.this, SelectShopBuyable.class)
                    .putExtra("SHOPID", shopID).putExtra("USERID", userID));
    }

    protected Barber setBarberFormValues(Integer _userID){
        Barber barber = new Barber();

        barber.setName(name.getText().toString());
        barber.setAlsohome( home.isChecked() ? 1 : 0);
        barber.setUserid(_userID.intValue());

        return barber;
    }

    protected User setUserFormValues(){
        User user = new User();
        user.setUsername(name.getText().toString()+"TMP");
        user.setUsertype(1);

        return user;
    }

    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }
}
