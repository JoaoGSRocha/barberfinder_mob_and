package com.codify.barberfinder_mob_and.Activities.SignIn_Register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.UserAPI;
import com.codify.barberfinder_mob_and.Activities.Create.Barber.CreateBarber;
import com.codify.barberfinder_mob_and.Activities.Create.Client.CreateClient;
import com.codify.barberfinder_mob_and.Activities.Create.Shop.CreateShop;
import com.codify.barberfinder_mob_and.Entities.User;
import com.codify.barberfinder_mob_and.R;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {


    EditText username, pw, email;
    Spinner usertype;
    private int userID;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        username = (EditText) findViewById(R.id.username_input);
        pw = (EditText) findViewById(R.id.pw_input);
        email = (EditText) findViewById(R.id.email_input);
        usertype = (Spinner) findViewById(R.id.register_spinner);

        intent = new Intent(Register.this, CreateShop.class);
        Button createAccountButton = (Button) findViewById(R.id.btnRegister);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                create();

            }
        });
    }

    protected void create(){

        httpOperations();
        User user = setUserFormValues();
        UserAPI userAPI =
                APIClient.getClient().create(UserAPI.class);

        userAPI.create(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    userID = response.body().getUserid();
                    if(response.body().getUsertype() == 0)
                        intent = new Intent(Register.this, CreateShop.class);
                    if(response.body().getUsertype() == 1)
                        intent = new Intent(Register.this, CreateBarber.class);
                    if(response.body().getUsertype() == 2)
                        intent = new Intent(Register.this, CreateClient.class);

                    intent.putExtra("USERID", userID);
                    startActivity(intent);

                    Log.i("###CRTID", String.valueOf(userID));
                }
                else
                    userID = -1;
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(Register.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected User setUserFormValues(){
        User user = new User();
        user.setUsername(username.getText().toString());
        user.setPw(pw.getText().toString());
        user.setEmail(email.getText().toString());
        user.setUsertype(usertype.getSelectedItemPosition());

        return user;
    }

    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }

}
