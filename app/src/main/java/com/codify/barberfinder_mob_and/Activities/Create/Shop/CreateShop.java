package com.codify.barberfinder_mob_and.Activities.Create.Shop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.ShopAPI;
import com.codify.barberfinder_mob_and.Entities.Shop;
import com.codify.barberfinder_mob_and.R;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateShop extends AppCompatActivity {
    EditText name, pw;
    private int shopID;
    private int usrID;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent pintent = getIntent();
        usrID = pintent.getIntExtra("USERID", -1);

        name = (EditText) findViewById(R.id.input_name);
        pw = (EditText) findViewById(R.id.input_pw);
        intent = new Intent(CreateShop.this, SelectShopLocation.class);
        Button createAccountButton = (Button) findViewById(R.id.btn_signup);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                create();

            }
        });
    }

    protected void create(){

        httpOperations();
        Shop shop = setShopFormValues();
        ShopAPI shopAPI =
                APIClient.getClient().create(ShopAPI.class);

        shopAPI.create(shop).enqueue(new Callback<Shop>() {
            @Override
            public void onResponse(Call<Shop> call, Response<Shop> response) {
                if(response.isSuccessful()) {
                    shopID = response.body().getShopid();
                    usrID = getIntent().getIntExtra("USERID", 0);
                    intent.putExtra("SHOPID", shopID);
                    intent.putExtra("USERID", usrID);
                    Log.i("###USERID", String.valueOf(usrID));

                    Log.i("###CRTID", String.valueOf(shopID));
                    startActivity(intent);
                }

                else
                   shopID = -1;

            }

            @Override
            public void onFailure(Call<Shop> call, Throwable t) {
                Toast.makeText(CreateShop.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected  Shop setShopFormValues(){
        Shop shop = new Shop();
        shop.setName(name.getText().toString());
        shop.setLocationid(2);
        usrID = getIntent().getIntExtra("USERID", 0);
        shop.setUserid(usrID);
        return shop;
    }



    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }

}
