package com.codify.barberfinder_mob_and.Activities.Create.Shop;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class SelectShopLocation extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {

    private GoogleMap mMap;
    private Marker marker;
    Intent pIntent;
    int shopID;
    int usrID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pIntent = getIntent();
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapLongClick(LatLng point) {
        AlertDialog.Builder setPasswordDialog = new AlertDialog.Builder(this);
        MarkerOptions options = new MarkerOptions()
                .position(point)
                .title("My Shop");

        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions()
                .position(
                        new LatLng(point.latitude,
                                point.longitude))
                .draggable(true).visible(true)
                .title("My Shop"));

        Toast.makeText(SelectShopLocation.this, String.valueOf(point.latitude)+String.valueOf(point.longitude), Toast.LENGTH_LONG);

        final Button button = findViewById(R.id.btn_Confirm);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SelectShopLocation.this, CreateShopBarber.class);
                intent.putExtra("LAT", String.valueOf(marker.getPosition().latitude));
                intent.putExtra("LNG", String.valueOf(marker.getPosition().longitude));
                shopID = pIntent.getIntExtra("SHOPID", 0);
                usrID = pIntent.getIntExtra("USERID", 0);
                Log.i("###MAPS ID", String.valueOf(shopID));
                Log.i("###USR ID", String.valueOf(usrID));
                intent.putExtra("SHOPID", shopID)
                    .putExtra("USERID", usrID);
                startActivity(intent);

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);
    }


    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
