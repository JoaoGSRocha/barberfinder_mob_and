package com.codify.barberfinder_mob_and.Activities.SignIn_Register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.UserAPI;
import com.codify.barberfinder_mob_and.Util.HttpOperations;
import com.codify.barberfinder_mob_and.Common;
import com.codify.barberfinder_mob_and.Entities.User;
import com.codify.barberfinder_mob_and.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity {


    EditText usr_input;
    EditText pw_input;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        usr_input = findViewById(R.id.user_input);
        pw_input = findViewById(R.id.pw_input);

        Button signin = findViewById(R.id.btn_signin);
        Button register = findViewById(R.id.btn_register);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignIn.this, Register.class));
            }
        });

        Common.currentToken = FirebaseInstanceId.getInstance().getToken();

        if(Common.currentToken != null)
            Log.d("MY TOKEN", Common.currentToken);

    }

    public void loadData(){
        final HttpOperations httpOperations = new HttpOperations();
        httpOperations.httpOperations();
        UserAPI userAPI =
                APIClient.getClient().create(UserAPI.class);
        userAPI.findAll().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                int usrId = getIntent().getIntExtra("USERID", 0);
                if(response.isSuccessful())
                    for(User user1 : response.body())
                        setRedirectValues().selectProfile(user1, usrId);
                else{
                    try {
                        httpOperations.inCaseOfErrorTry(SignIn.this, response);

                    } catch (Exception e) {
                        httpOperations.inCaseOfErrorCatch(SignIn.this, e);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(SignIn.this, t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private SignInRedirect setRedirectValues(){
        SignInRedirect redirect = new SignInRedirect(pw_input, usr_input,
                SignIn.this);
        return redirect;
    }
}
