package com.codify.barberfinder_mob_and.Activities.Edit_Confirmation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.BarberAPI;
import com.codify.barberfinder_mob_and.API.ShopBarberAPI;
import com.codify.barberfinder_mob_and.Util.BarberEditAdapter;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.Entities.ShopBarber;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditBarbers extends AppCompatActivity {

    private int shopID;
    private int userID;
    private static ArrayList<Barber> barbers_al;

    public static ArrayList<Barber> getBarbers_al() {
        return barbers_al;
    }

    public void setBarbers_al(ArrayList<Barber> barbers_al) {
        this.barbers_al = barbers_al;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_barbers);
        barbers_al = new ArrayList<Barber>();
        loadData();
    }

    public void loadData() {
        try {
            ShopBarberAPI shopBarberAPI =
                    APIClient.getClient().create(ShopBarberAPI.class);
            shopBarberAPI.findAll().enqueue(new Callback<List<ShopBarber>>() {
                @Override
                public void onResponse(Call<List<ShopBarber>> call, Response<List<ShopBarber>> response) {
                    if (response.isSuccessful()) {
                        for (ShopBarber shopBarber : response.body()) {
                            if (shopBarber.getShopid() == 1) {
                                Log.i("###1stTIme", String.valueOf(shopBarber.getBarberid()));
                                ListBarbers(shopBarber.getBarberid());
                            }
                        }
                        setBarbers_al(barbers_al);
                        ListView();
                    }
                }
                @Override
                public void onFailure(Call<List<ShopBarber>> call, Throwable t) {
                }
            });
        }catch(Exception e) {
            Toast.makeText(EditBarbers.this, e.getMessage(), Toast.LENGTH_SHORT);
        }
    }

    private void ListBarbers(int id) {
        BarberAPI barberAPI =
                APIClient.getClient().create(BarberAPI.class);
        barberAPI.find(id).enqueue(new Callback<Barber>() {
            @Override
            public void onResponse(Call<Barber> call,
                                   Response<Barber> response) {
                if (response.isSuccessful()) {
                    barbers_al = getBarbers_al();
                    barbers_al.add((Barber) response.body());
                    setBarbers_al(barbers_al);
                } else {
                    Log.i("###BarberListFailed", "Failed");
                }
            }

            @Override
            public void onFailure(Call<Barber> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(EditBarbers.this, t.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void ListView(){

        //Log.i("Test", barbers_al.get(0).toString());
        ListAdapter barberAdapter = new BarberEditAdapter(this, barbers_al);
        ListView barberaListView = (ListView) findViewById(R.id.editBarberListView);
        barberaListView.setAdapter(barberAdapter);
        barberaListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Barber barber = (Barber) parent.getItemAtPosition(position);
                        Toast.makeText(EditBarbers.this, barber.toString(), Toast.LENGTH_SHORT).show();

                    }
                }
        );
    }
}
