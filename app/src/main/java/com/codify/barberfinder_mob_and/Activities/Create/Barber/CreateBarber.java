package com.codify.barberfinder_mob_and.Activities.Create.Barber;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.BarberAPI;
import com.codify.barberfinder_mob_and.Util.HttpOperations;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.Fragments.Barber.BarberMain;
import com.codify.barberfinder_mob_and.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateBarber extends AppCompatActivity {
    EditText name;
    Switch home;
    Integer usrID;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_barber);
        usrID = getIntent().getIntExtra("USERID", 0);

        name = (EditText) findViewById(R.id.barbername_input);
        home = (Switch) findViewById(R.id.barberhome_switch);
        intent = new Intent(CreateBarber.this, BarberMain.class);
        intent.putExtra("USERID", usrID);
        Button createAccountButton = (Button) findViewById(R.id.button_barber_register);
        createAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                create();

            }
        });
    }

    public void create(){
        final HttpOperations httpOperations = new HttpOperations();
        httpOperations.httpOperations();
        Barber barber = setBarberFormValues();
        BarberAPI barberAPI = APIClient.getClient().create(BarberAPI.class);

        barberAPI.create(barber).enqueue(new Callback<Barber>() {
            @Override
            public void onResponse(Call<Barber> call, Response<Barber> response) {
                if(response.isSuccessful()) {
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Barber> call, Throwable t) {

            }
        });

    }

    private Barber setBarberFormValues() {
        Barber barber = new Barber();
        barber.setName(name.getText().toString());
        barber.setAlsohome(home.isChecked() ? 1 : 0);
        barber.setUserid(usrID);
        return  barber;
    }
}
