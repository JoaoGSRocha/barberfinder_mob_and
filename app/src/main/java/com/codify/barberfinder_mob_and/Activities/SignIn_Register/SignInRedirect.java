package com.codify.barberfinder_mob_and.Activities.SignIn_Register;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;

import com.codify.barberfinder_mob_and.Entities.User;
import com.codify.barberfinder_mob_and.Fragments.Barber.BarberMain;
import com.codify.barberfinder_mob_and.Fragments.Client.ClientMain;
import com.codify.barberfinder_mob_and.Fragments.Shop.ShopMain;

public class SignInRedirect {

    Intent intent = new Intent();
    Context prior;
    String pw, usr = "";

    public void setIntent(Intent intent) { this.intent = intent; }

    protected SignInRedirect(EditText pw_input, EditText usr_input, Context context){
        pw = pw_input.getText().toString();
        usr = usr_input.getText().toString();
        prior = context;
    }

    public void selectProfile(User user, int usrID){
            if (user.getPw() != null
                    && user.getUsername()!= null
                    && user.getPw().equals(pw) && user.getUsername().equals
                    (usr)) {
                switch (user.getUsertype()) {
                    case 0:
                        intent = new Intent(prior, ShopMain.class);
                        break;
                    case 1:
                        intent = new Intent(prior, BarberMain.class);
                        break;
                    case 2:
                        intent = new Intent(prior, ClientMain.class);
                        break;
                }
                intent.putExtra("USERID", usrID);
                prior.startActivity(intent);
            }
    }
}