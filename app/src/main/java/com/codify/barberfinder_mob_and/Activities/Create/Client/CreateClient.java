package com.codify.barberfinder_mob_and.Activities.Create.Client;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.ClientAPI;
import com.codify.barberfinder_mob_and.Util.HttpOperations;
import com.codify.barberfinder_mob_and.Entities.Client;
import com.codify.barberfinder_mob_and.Fragments.Client.ClientMain;
import com.codify.barberfinder_mob_and.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateClient extends AppCompatActivity {
    EditText name;
    EditText mobile;
    private int usrID;
    private int clientID;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_client);
        Intent pintent = getIntent();
        usrID = pintent.getIntExtra("USERID", -1);

        name = (EditText) findViewById(R.id.name_input);
        mobile = (EditText) findViewById(R.id.mobile_input);
        intent = new Intent(CreateClient.this, ClientMain.class);
        Button createClientButton = (Button) findViewById(R.id.btnClientConfirm);
        createClientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                create();
            }
        });
    }

    protected void create(){
        final HttpOperations httpOperations = new HttpOperations();
        httpOperations.httpOperations();
        Client client = setClientFormValues();
        ClientAPI clientAPI =
                APIClient.getClient().create(ClientAPI.class);

        clientAPI.create(client).enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if(response.isSuccessful()){
                    clientID = response.body().getClientid();
                    nextActivity();
                } else{
                    try {
                        httpOperations.inCaseOfErrorTry(CreateClient.this, response);

                    } catch (Exception e) {
                        httpOperations.inCaseOfErrorCatch(CreateClient.this, e);
                    }
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                Toast.makeText(CreateClient.this, t.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void nextActivity() {
        usrID = getIntent().getIntExtra("USERID", 0);
        intent.putExtra("CLIENTID", clientID);
        intent.putExtra("USERID", usrID);
        startActivity(intent);
    }

    private Client setClientFormValues() {
        Client client = new Client();
        client.setName(name.getText().toString());
        client.setMobile(Integer.parseInt(mobile.getText().toString()));
        usrID = getIntent().getIntExtra("USERID", 0);
        client.setUserid(usrID);
        return client;
    }
}
