package com.codify.barberfinder_mob_and.Remote;


import com.codify.barberfinder_mob_and.Model.MyResponse;
import com.codify.barberfinder_mob_and.Model.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers({"Content-Type: application/json",
            "Authorization:key=AAAAqN5on3I:APA91bG5doFAonsi3GLQmoNeMZYb17-NN8" +
                    "bNHvJXIFyfUdG0RaLxX8KJ9xjrrqANpInZbD5ybzqWPOr_RTc7GA0_Cn" +
                    "imUx3yVQUwYjzAr7wlInTKg61YQ-710Rxo9C6geirfATI1Jxa6"})
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
