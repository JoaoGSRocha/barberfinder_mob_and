package com.codify.barberfinder_mob_and;

import com.codify.barberfinder_mob_and.Remote.APIService;
import com.codify.barberfinder_mob_and.Remote.RetrofitClient;

public class Common {
    public static String currentToken = "";

    private static String baseUrl = "https://fcm.googleapis.com/";

    public static APIService getFCMClient()
    {
        return RetrofitClient.getClient(baseUrl).create(APIService.class);
    }
}
