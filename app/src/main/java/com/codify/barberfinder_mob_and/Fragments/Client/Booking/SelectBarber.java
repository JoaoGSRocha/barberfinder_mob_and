package com.codify.barberfinder_mob_and.Fragments.Client.Booking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.BarberAPI;
import com.codify.barberfinder_mob_and.API.ShopBarberAPI;
import com.codify.barberfinder_mob_and.Activities.Create.Shop.SelectShopBuyable;
import com.codify.barberfinder_mob_and.Util.BarberSelectAdapter;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.Entities.Shop;
import com.codify.barberfinder_mob_and.Entities.ShopBarber;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectBarber extends AppCompatActivity {

    ShopBarberAPI barberBarberAPI;
    private static final ArrayList<Barber> barbers_al = new ArrayList<Barber>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_barber);
        barbers_al.clear();
        loadData();
    }

    public void loadData() {
        try {
            BarberAPI BarberAPI =
                    APIClient.getClient().create(BarberAPI.class);
            BarberAPI.findAll().enqueue(new Callback<List<Barber>>() {
                @Override
                public void onResponse(Call<List<Barber>> call,
                                       Response<List<Barber>> response) {
                    if(response.isSuccessful()) {
                        ArrayList<Barber> barber_al_all = new ArrayList<Barber>();
                        for (int i = 0; i < response.body().size(); i++) {
                            Barber barber = response.body().get(i);
                            barber_al_all.add(barber);
                        }
                        loadBarbers(barber_al_all);
                    }
                }

                @Override
                public void onFailure(Call<List<Barber>> call,
                                      Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Toast.makeText(SelectBarber.this, e.getMessage(), Toast.LENGTH_SHORT);
        }


    }

    public void loadBarbers(final ArrayList<Barber> barber_all){
        barberBarberAPI =
                APIClient.getClient().create(ShopBarberAPI.class);
        barberBarberAPI.findAll().enqueue(new Callback<List<ShopBarber>>() {
            @Override
            public void onResponse(Call<List<ShopBarber>> call, Response<List<ShopBarber>> response) {


                for (ShopBarber barberBarber : response.body())
                    for (Barber barber : barber_all)
                        if (barberBarber.getShopid().equals(barber.getBarberid()))
                            barbers_al.add(barber);

                ListView();
            }

            @Override
            public void onFailure(Call<List<ShopBarber>> call, Throwable t) { Log.i("###Shops_al", t.toString());
            }
        });
    }

  private void ListView(){

        long time = getIntent().getLongExtra("TIME", 0L);
        int shopid = getIntent().getIntExtra("SHOPID", 0);
        int shopslotid = getIntent().getIntExtra("SHOPSLOTID", 0);
        final int usrID = getIntent().getIntExtra("USERID", 0);
        Log.i("###TIME", time+" shopid "+shopid);
       ListAdapter barberAdapter = new BarberSelectAdapter(this, barbers_al, time, shopid,
                shopslotid, usrID);
        ListView barberListView = (ListView) findViewById(R.id.barberListView);
        barberListView.setAdapter(barberAdapter);
        barberListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Shop barber = (Shop) parent.getItemAtPosition(position);
                        Toast.makeText(SelectBarber.this, barber.toString(),
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SelectBarber.this, SelectShopBuyable.class);
                        intent.putExtra("BarberID",barber.getShopid());
                        intent.putExtra("USRID", usrID);
                        startActivity(intent);
                    }
                }
        );
    }
}
