package com.codify.barberfinder_mob_and.Fragments.Client;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.AppointmentAPI;
import com.codify.barberfinder_mob_and.API.BarberAPI;
import com.codify.barberfinder_mob_and.API.ClientAPI;
import com.codify.barberfinder_mob_and.API.ShopAPI;
import com.codify.barberfinder_mob_and.API.ShopSlotAPI;
import com.codify.barberfinder_mob_and.API.UserAPI;
import com.codify.barberfinder_mob_and.Entities.Appointment;
import com.codify.barberfinder_mob_and.Entities.Barber;
import com.codify.barberfinder_mob_and.Entities.Client;
import com.codify.barberfinder_mob_and.Entities.Shop;
import com.codify.barberfinder_mob_and.Entities.ShopSlot;
import com.codify.barberfinder_mob_and.Entities.User;
import com.codify.barberfinder_mob_and.R;
import com.codify.barberfinder_mob_and.Util.AppointmentQueue.AppointmentAdapter;
import com.codify.barberfinder_mob_and.Util.HttpOperations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ClientAppointments extends Fragment {

    HttpOperations httpOperations = new HttpOperations();
    View rootView;

    public ClientAppointments() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_client_appointments, container, false);
        //loadShopSlot();
        Client client = new Client();
        client.setUserid(2);
        loadClientAppointments(client);
        //loadUser();
        return rootView;
    }

    private void loadUser(){
        httpOperations.httpOperations();
        final User user = setUserFormValues(2);
        UserAPI userAPI =
                APIClient.getClient().create(UserAPI.class);
        userAPI.find(2).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                loadClient(user);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void loadClient(final User user) {
        ClientAPI clientAPI =
                APIClient.getClient().create(ClientAPI.class);
        clientAPI.findAll().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                if(response.isSuccessful()){
                    for(Client client : response.body()){
                        if(client.getUserid().equals(user.getUserid()));
                        loadClientAppointments(client);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {

            }
        });

    }

    private void loadClientAppointments(final Client client){
        AppointmentAPI appointmentAPI =
                APIClient.getClient().create(AppointmentAPI.class);
        appointmentAPI.findAll().enqueue(new Callback<List<Appointment>>() {
            @Override
            public void onResponse(Call<List<Appointment>> call, Response<List<Appointment>> response) {
                ArrayList<Appointment> appointment_al = new ArrayList<Appointment>();
                if(response.isSuccessful()){
                    for(Appointment appointment : response.body()){
                            appointment_al.add(appointment);
                    }
                    loadShopNames(appointment_al, client);
                }
            }

            @Override
            public void onFailure(Call<List<Appointment>> call, Throwable t) {

            }
        });
    }

    private void loadShopNames(final ArrayList<Appointment> appointment_al, final Client client){
        ShopAPI shopAPI =
                APIClient.getClient().create(ShopAPI.class);
        shopAPI.findAll().enqueue(new Callback<List<Shop>>() {
            @Override
            public void onResponse(Call<List<Shop>> call, Response<List<Shop>> response) {
                HashMap<Integer, String> shopName_hm = new HashMap<Integer, String>();
                if(response.isSuccessful())
                    for(Shop shop : response.body())
                        shopName_hm.put(shop.getShopid(), shop.getName());
                loadBarberNames(appointment_al, shopName_hm, client);

            }

            @Override
            public void onFailure(Call<List<Shop>> call, Throwable t) {

            }
        });
    }

    private void loadBarberNames(final ArrayList<Appointment> appointment_al, final
    HashMap<Integer, String> shopName_hm, final Client client) {
        BarberAPI barberAPI =
                APIClient.getClient().create(BarberAPI.class);
        barberAPI.findAll().enqueue(new Callback<List<Barber>>() {
            @Override
            public void onResponse(Call<List<Barber>> call, Response<List<Barber>> response) {
               HashMap<Integer,String> barberName_hm = new HashMap<Integer, String>();
               if(response.isSuccessful())
                   for(Barber barber : response.body())
                       barberName_hm.put(barber.getBarberid(), barber.getName());
               loadShopSlot(appointment_al, shopName_hm, barberName_hm, client);
            }

            @Override
            public void onFailure(Call<List<Barber>> call, Throwable t) {

            }
        });
    }

    private void loadShopSlot(final ArrayList<Appointment> appointment_al,
                              final HashMap<Integer, String> shopName_hm,
                              final HashMap<Integer, String> barber_hm,
                              final Client client) {
        ShopSlotAPI shopSlotAPI =
                APIClient.getClient().create(ShopSlotAPI.class);
        shopSlotAPI.findAll().enqueue(new Callback<List<ShopSlot>>() {
            @Override
            public void onResponse(Call<List<ShopSlot>> call, Response<List<ShopSlot>> response) {
                HashMap<Integer, Integer> shopSlot_hm = new HashMap<Integer, Integer>();
                if(response.isSuccessful()){
                    for(ShopSlot shopSlot : response.body()){
                        for(Appointment appointment : appointment_al){
                            if(shopSlot.getAppointmentid() == appointment.getAppointmentid() ) {
                                shopSlot_hm.put(shopSlot.getDayid(), shopSlot.getShopid());
                            }
                        }
                    }
                    ListView(appointment_al, shopName_hm, barber_hm, shopSlot_hm, client.getUserid());
                }
            }

            @Override
            public void onFailure(Call<List<ShopSlot>> call, Throwable throwable) {

            }
        });
    }

    private User setUserFormValues(int userid) {
        User user = new User();
        user.setUserid(userid);
        return user;
    }

    //The problem is no in the Adapter or in any of the layouts....
    //its in this class loading...figure out what it is.
    //When it sends to the adapter nothing comes out, even if it does
    //try to show just the template layout using none of the data
    //this looks as if it is not managing to load/add any value to
    //the ArrayList<Appointments> at all...
    //TODO: It is working as of now...now have to load
    //their respective data, time, date, ....
    private void ListView(ArrayList<Appointment> appointments_al,
                          HashMap<Integer, String> shopName_hm,
                          HashMap<Integer, String> barberName_hm,
                          HashMap<Integer, Integer> shopSlot_hm, Integer userid){
        ArrayList<Appointment> appointments = new ArrayList<Appointment>();
        AppointmentAdapter appointmentAdapter
                = new AppointmentAdapter(getActivity(), appointments_al, shopName_hm,
                barberName_hm, shopSlot_hm, userid);
        ListView appointmentList = (ListView) rootView.findViewById(R.id.list_appointment_queue);

        appointmentList.setAdapter(appointmentAdapter);
    }


}
