package com.codify.barberfinder_mob_and.Fragments.Barber;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codify.barberfinder_mob_and.R;

public class BarberAppointments extends Fragment {

    public BarberAppointments() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_barber_appointments, container, false);
    }
}
