package com.codify.barberfinder_mob_and.Fragments.Client.Booking.Timetable;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.DayOfWeekAPI;
import com.codify.barberfinder_mob_and.API.DaySlotAPI;
import com.codify.barberfinder_mob_and.API.ShopSlotAPI;
import com.codify.barberfinder_mob_and.Util.ClientAppointment.ShopSlotAdapter;
import com.codify.barberfinder_mob_and.Entities.DayOfWeek;
import com.codify.barberfinder_mob_and.Entities.DaySlot;
import com.codify.barberfinder_mob_and.Entities.ShopSlot;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDaySlot extends AppCompatActivity {
    public static int test = 0;
    Intent pIntent;
    int shopID = 0;
    int currWODID = 0;

    private static ArrayList<ShopSlot> shopSlots_al;

    public static ArrayList<ShopSlot> getShopSlots_al() {
        return shopSlots_al;
    }

    public static ArrayList<DaySlot> daySlot_al;

    public static ArrayList<DaySlot> getDaySlot_al() {
        return daySlot_al;
    }

    public static final ArrayList<ShopSlot> newshopSlot_al = new ArrayList<ShopSlot>();

    public static void setDaySlot_al(ArrayList<DaySlot> daySlot_al) {
        ViewDaySlot.daySlot_al = daySlot_al;
    }

    public static void setShopSlots_al(ArrayList<ShopSlot> shopSlots_al) {
        ViewDaySlot.shopSlots_al = shopSlots_al;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shopSlots_al = new ArrayList<ShopSlot>();
        daySlot_al = new ArrayList<DaySlot>();
        setContentView(R.layout.activity_view_day_slot);
        pIntent = getIntent();
        loadDaysOfWeek(currWODID);
    }

    @Override
    public void onResume() {
        super.onResume();
        newshopSlot_al.clear();
    }

    //REST Request to get all the Slots of the ShopSlot for the Shop we are querrying
    //In future this loadData will be loadData(Shop shop) to define the shopid for
    // the ShopSlot ArrayList that you wanna filter these with
    //Right now it does not even have the filter that is yet to be added.
    public void loadData(final ArrayList<DayOfWeek> dayOfWeeks, final int currWODID) {
        try {
            ShopSlotAPI ShopSlotAPI =
                    APIClient.getClient().create(ShopSlotAPI.class);
            ShopSlotAPI.findAll().enqueue(new Callback<List<ShopSlot>>() {
                @Override
                public void onResponse(Call<List<ShopSlot>> call,
                                       Response<List<ShopSlot>> response) {
                    if(response.isSuccessful()) {
                        for(int i = 0; i < response.body().size(); i++){
                            shopID = pIntent.getIntExtra("SHOPID", -1);
                            ShopSlot shopSlot = response.body().get(i);
                            if(shopID != -1 && shopID == Integer
                                    .valueOf(shopSlot.getShopid())) {
                                if (null != shopSlot)
                                    shopSlots_al.add(response.body().get(i));
                            }
                        }
                        setShopSlots_al(shopSlots_al);
                        sendDataToPopulate(shopSlots_al, shopID, dayOfWeeks,
                                currWODID);
                    }
                    else{ }
                }

                @Override
                public void onFailure(Call<List<ShopSlot>> call,
                                      Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Toast.makeText(ViewDaySlot.this, e.getMessage(), Toast.LENGTH_SHORT);
        }
    }

    public void loadDaysOfWeek(final int currWOD){

        try{
            DayOfWeekAPI dayOfWeekAPI =
                    APIClient.getClient().create(DayOfWeekAPI.class);
            dayOfWeekAPI.findAll().enqueue(new Callback<List<DayOfWeek>>() {
                ArrayList<DayOfWeek> dayOfWeek_al = new ArrayList<DayOfWeek>();
                @Override
                public void onResponse(Call<List<DayOfWeek>> call, Response<List<DayOfWeek>> response) {
                    if(response.isSuccessful()){
                        dayOfWeek_al = (ArrayList<DayOfWeek>) response.body();
                        loadData(dayOfWeek_al, currWODID);
                    }
                }

                @Override
                public void onFailure(Call<List<DayOfWeek>> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(ViewDaySlot.this, e.getMessage(), Toast.LENGTH_LONG);
        }

    }

    //REST Request to list all DaySlots to a new ArrayList
    public void sendDataToPopulate(final ArrayList<ShopSlot> shopSlots,
                                   final int shopID,
                                   final ArrayList<DayOfWeek> dayOfWeeks,
                                   final int currWODID){
        DaySlotAPI daySlotAPI =
                APIClient.getClient().create(DaySlotAPI.class);
        daySlotAPI.findAll().enqueue(new Callback<List<DaySlot>>() {
            @Override
            public void onResponse(Call<List<DaySlot>> call, Response<List<DaySlot>> response) {
                for(DaySlot day : response.body()){
                    daySlot_al.add(day);
                }
                sort_complete_ShopSlots(shopSlots, daySlot_al, shopID,
                        dayOfWeeks, currWODID);
            }

            @Override
            public void onFailure(Call<List<DaySlot>> call, Throwable t) {
                Log.e("Failure", t.toString());
            }
        });
    }

    //List all existant ShopSlots for the Shop, & in a third loadData()
    // lay out all the Dayslots and send it back to this sendDataToPopulate() to
    // compare the ShopSlots listed here against Dayslots...
    // per each index of DaySlots loop through the ShopSlots
    // to check whether or not it has the timetxt of that index
    // then if it has add it and paste it onto a new Arraylist
    // so it can be sorted...and if there is not create one
    // with that timetxt value
    //It will have to send the final result of this value to the
    // ShopSlotAdapter which has to go back to how it was originally
    //where then is simply doing what it does normally and list
    //these values.
    synchronized public void sort_complete_ShopSlots(ArrayList<ShopSlot> shopSlots,
                                                     ArrayList<DaySlot>
                                                             daySlots,
                                                     final int shopID,
                                                     ArrayList<DayOfWeek> dayOfWeeks,
                                                     int currWODID) {
        boolean thereis = false;
        for(final DaySlot day : daySlots){
            for(ShopSlot shopSlot : shopSlots){
                if( shopSlot.getSlotid().equals( day.getSlotid() ) ){
                    if(shopSlot.getDayid().equals(currWODID)) {
                        shopSlot.setTimetxt(day.getTime());
                        newshopSlot_al.add(shopSlot);
                        thereis = true;
                    }
                }
            }

            final boolean finalThereis = thereis;
            ViewDaySlot.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!finalThereis)
                        createShopSlot(day.getTime(), day.getSlotid(), shopID);
                    if (day.getTime().equals("23:30"))
                        ListView();
                }
            });

            thereis = false;
        }

    }

    public void createShopSlot(final String timetxt, final Integer slotid, int shopID){
        httpOperations();
        ShopSlot shopSlot = new ShopSlot();
        shopSlot.setTimetxt(timetxt);
        shopSlot.setShopid(shopID); //In the final version this will have to be given in
        shopSlot.setSlotid(slotid); //In the final version this will have to be given in
        //the params, and in a chain of methods sending to one another from
        //the original loadData()

        ShopSlotAPI shopSlotAPI =
                APIClient.getClient().create(ShopSlotAPI.class);

        shopSlotAPI.create(shopSlot).enqueue(new Callback<ShopSlot>() {
            @Override
            public void onResponse(Call<ShopSlot> call, Response<ShopSlot> response) {
                ShopSlot _shopSlot = response.body();
                newshopSlot_al.add(_shopSlot);
            }

            @Override
            public void onFailure(Call<ShopSlot> call, Throwable t) {
                Log.e("Failure", t.toString());
            }
        });
    }

    private void ListView(){

        Log.i("Test", newshopSlot_al.get(0).toString());
        int usrID = getIntent().getIntExtra("USERID", 0);
        ListAdapter ShopSlotAdapter =
                new ShopSlotAdapter(this, newshopSlot_al, usrID, getIntent().getLongExtra("TIME", 0));
        ListView buyaList = (ListView) findViewById(R.id.dailySlotListView);
        buyaList.setAdapter(ShopSlotAdapter);
        buyaList.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ShopSlot shopSlot = (ShopSlot) parent.getItemAtPosition(position);
                        Toast.makeText(ViewDaySlot.this, shopSlot.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }
}
