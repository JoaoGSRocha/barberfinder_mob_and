package com.codify.barberfinder_mob_and.Fragments.Client.Booking.Timetable;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.DayOfWeekAPI;
import com.codify.barberfinder_mob_and.API.DaySlotAPI;
import com.codify.barberfinder_mob_and.API.ShopSlotAPI;
import com.codify.barberfinder_mob_and.Entities.DayOfWeek;
import com.codify.barberfinder_mob_and.Entities.DaySlot;
import com.codify.barberfinder_mob_and.Entities.ShopSlot;
import com.codify.barberfinder_mob_and.R;
import com.codify.barberfinder_mob_and.Util.ClientAppointment.ShopSlotAdapter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeekDay extends Fragment {

    Intent pIntent;
    int shopID = 0;
    int currWODID = 0;
    View rootView;
    View shopslot_rowView;

    private static ArrayList<ShopSlot> shopSlots_al;

    public static ArrayList<ShopSlot> getShopSlots_al() {
        return shopSlots_al;
    }

    public static ArrayList<DaySlot> daySlot_al;

    public static ArrayList<DaySlot> getDaySlot_al() {
        return daySlot_al;
    }

    public static final ArrayList<ShopSlot> newshopSlot_al = new ArrayList<ShopSlot>();

    public static void setDaySlot_al(ArrayList<DaySlot> daySlot_al) {
        WeekDay.daySlot_al = daySlot_al;
    }

    public static void setShopSlots_al(ArrayList<ShopSlot> shopSlots_al) {
        WeekDay.shopSlots_al = shopSlots_al;
    }

    public WeekDay() {
    }


    //gonna receive from getArguments() val, saying what day of the week it is
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        shopID = getArguments().getInt("SHOPID");
        rootView = inflater.inflate(R.layout.fragment_week_day,
                container, false);
        shopSlots_al = new ArrayList<ShopSlot>();
        daySlot_al = new ArrayList<DaySlot>();

        String DOW = "";
        try {
            if (getArguments().getString("DOW") != null) {
                DOW = getArguments().getString("DOW").toUpperCase();
                switch (DOW) {
                    case "MONDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 1;
                        break;
                    case "TUESDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 2;
                        break;
                    case "WEDNESDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 3;
                        break;
                    case "THURSDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 4;
                        break;
                    case "FRIDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 5;
                        break;
                    case "SATURDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 6;
                        break;
                    case "SUNDAY":
                        Toast.makeText(getContext(), DOW, Toast.LENGTH_SHORT).show();
                        currWODID = 7;
                        break;
                }
            }
        }
        catch (Exception e){

        }
        loadDaysOfWeek(currWODID);

        // Inflate the layout for this fragment
        return rootView;
    }

    public void loadData(final ArrayList<DayOfWeek> dayOfWeeks, final int currWODID) {
        try {
            ShopSlotAPI ShopSlotAPI =
                    APIClient.getClient().create(ShopSlotAPI.class);
            ShopSlotAPI.findAll().enqueue(new Callback<List<ShopSlot>>() {
                @Override
                public void onResponse(Call<List<ShopSlot>> call,
                                       Response<List<ShopSlot>> response) {
                    if(response.isSuccessful()) {
                        for(int i = 0; i < response.body().size(); i++){
                            ShopSlot shopSlot = response.body().get(i);
                            if(shopID != -1 && shopID == Integer
                                    .valueOf(shopSlot.getShopid())) {
                                if (null != shopSlot)
                                    shopSlots_al.add(response.body().get(i));
                            }
                        }
                        setShopSlots_al(shopSlots_al);
                        sendDataToPopulate(shopSlots_al, shopID, dayOfWeeks,
                                currWODID);
                    }
                    else{ }
                }

                @Override
                public void onFailure(Call<List<ShopSlot>> call,
                                      Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT);
        }
    }

    public void loadDaysOfWeek(final int currWOD){

        try{
            DayOfWeekAPI dayOfWeekAPI =
                    APIClient.getClient().create(DayOfWeekAPI.class);
            dayOfWeekAPI.findAll().enqueue(new Callback<List<DayOfWeek>>() {
                ArrayList<DayOfWeek> dayOfWeek_al = new ArrayList<DayOfWeek>();
                @Override
                public void onResponse(Call<List<DayOfWeek>> call, Response<List<DayOfWeek>> response) {
                    if(response.isSuccessful()){
                        dayOfWeek_al = (ArrayList<DayOfWeek>) response.body();
                        loadData(dayOfWeek_al, currWODID);
                    }
                }

                @Override
                public void onFailure(Call<List<DayOfWeek>> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG);
        }

    }

    //REST Request to list all DaySlots to a new ArrayList
    public void sendDataToPopulate(final ArrayList<ShopSlot> shopSlots,
                                   final int shopID,
                                   final ArrayList<DayOfWeek> dayOfWeeks,
                                   final int currWODID){
        DaySlotAPI daySlotAPI =
                APIClient.getClient().create(DaySlotAPI.class);
        daySlotAPI.findAll().enqueue(new Callback<List<DaySlot>>() {
            @Override
            public void onResponse(Call<List<DaySlot>> call, Response<List<DaySlot>> response) {
                for(DaySlot day : response.body()){
                    daySlot_al.add(day);
                }
                sort_complete_ShopSlots(shopSlots, daySlot_al, shopID,
                        dayOfWeeks, currWODID);
            }

            @Override
            public void onFailure(Call<List<DaySlot>> call, Throwable t) {
                Log.e("Failure", t.toString());
            }
        });
    }

    //List all existant ShopSlots for the Shop, & in a third loadData()
    // lay out all the Dayslots and send it back to this sendDataToPopulate() to
    // compare the ShopSlots listed here against Dayslots...
    // per each index of DaySlots loop through the ShopSlots
    // to check whether or not it has the timetxt of that index
    // then if it has add it and paste it onto a new Arraylist
    // so it can be sorted...and if there is not create one
    // with that timetxt value
    //It will have to send the final result of this value to the
    // ShopSlotAdapter which has to go back to how it was originally
    //where then is simply doing what it does normally and list
    //these values.
    synchronized public void sort_complete_ShopSlots(ArrayList<ShopSlot> shopSlots,
                                                     ArrayList<DaySlot>
                                                             daySlots,
                                                     final int shopID,
                                                     ArrayList<DayOfWeek> dayOfWeeks,
                                                     final int currWODID) {
        final int[] counterThereIsNot = {0};
        boolean thereis = false;
        for(final DaySlot day : daySlots){
            for(ShopSlot shopSlot : shopSlots){
                if( shopSlot.getSlotid().equals( day.getSlotid() ) ){
                    if(shopSlot.getDayid().equals(currWODID)) {
                        shopSlot.setTimetxt(day.getTime());
                        newshopSlot_al.add(shopSlot);
                        thereis = true;
                    }
                }
            }
            if(currWODID == 4){
                Log.i("###WEDN","It is Wednesday");
            }
            final boolean finalThereis = thereis;
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (!finalThereis)
                        counterThereIsNot[0]++;
                        createShopSlot(day.getTime(), day.getSlotid(), shopID, currWODID);
                    if (day.getTime().equals("23:30") && counterThereIsNot[0] == 0)
                        ListView();
                }
            });

            thereis = false;
        }

    }

    public void createShopSlot(final String timetxt, final Integer slotid, int shopID, int dayID){
        httpOperations();
        ShopSlot shopSlot = new ShopSlot();
        shopSlot.setTimetxt(timetxt);
        shopSlot.setShopid(shopID); //In the final version this will have to be given in
        shopSlot.setSlotid(slotid); //In the final version this will have to be given in
        shopSlot.setDayid(dayID); //In the final version this will have to be given in
        //the params, and in a chain of methods sending to one another from
        //the original loadData()

        ShopSlotAPI shopSlotAPI =
                APIClient.getClient().create(ShopSlotAPI.class);

        shopSlotAPI.create(shopSlot).enqueue(new Callback<ShopSlot>() {
            @Override
            public void onResponse(Call<ShopSlot> call, Response<ShopSlot> response) {
                ShopSlot _shopSlot = response.body();
                newshopSlot_al.add(_shopSlot);
            }

            @Override
            public void onFailure(Call<ShopSlot> call, Throwable t) {
                Log.e("Failure", t.toString());
            }
        });
    }

    private void ListView(){
        int usrID = getArguments().getInt("USERID", 0);

        ListAdapter ShopSlotAdapter =
                new ShopSlotAdapter(getActivity(), newshopSlot_al, usrID, getArguments().getLong
                        ("TIME", 0));
        ListView buyaList = (ListView) rootView.findViewById(R.id.dailySlotListView);
        buyaList.setAdapter(ShopSlotAdapter);
    }

    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }

}
