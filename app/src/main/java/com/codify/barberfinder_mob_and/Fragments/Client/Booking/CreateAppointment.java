package com.codify.barberfinder_mob_and.Fragments.Client.Booking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.AppointmentAPI;
import com.codify.barberfinder_mob_and.API.ClientAPI;
import com.codify.barberfinder_mob_and.API.ShopSlotAPI;
import com.codify.barberfinder_mob_and.Entities.Appointment;
import com.codify.barberfinder_mob_and.Entities.Client;
import com.codify.barberfinder_mob_and.Entities.ShopSlot;
import com.codify.barberfinder_mob_and.Fragments.Client.ClientMain;
import com.codify.barberfinder_mob_and.R;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAppointment extends AppCompatActivity {

    EditText name;
    Integer shopID;
    Integer barberid;
    Integer userID;
    Integer buyableid;
    Integer shopslotid;
    public static Integer clientID;
    long time;
    boolean repeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appointment);


        //TODO NEXT
        //clientID = getIntent().getIntExtra("CLIENTID", 0);
        name = (EditText) findViewById(R.id.input_name);
        shopID = getIntent().getIntExtra("SHOPID", 0);
        userID = getIntent().getIntExtra("USERID", 0);
        time = getIntent().getLongExtra("TIME", 0L);
        barberid = getIntent().getIntExtra("BARBERID", 0);
        buyableid = getIntent().getIntExtra("BUYABLEID", 0);
        shopslotid = getIntent().getIntExtra("SHOPSLOTID", 0);


        Log.i("###SHOPSLOTID", String.valueOf(shopslotid));

        loadClientID();


        Button back = (Button) findViewById(R.id.btnHomePage);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CreateAppointment.this, ClientMain.class));
            }
        });

    }

    protected void createAppointment(){

        httpOperations();
        Appointment appointment = setAppointmentFormValues();
        AppointmentAPI appointmentAPI =
                APIClient.getClient().create(AppointmentAPI.class);
        appointmentAPI.create(appointment).enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {
                if(response.isSuccessful()){
                    loadShopSlot(response.body().getAppointmentid());
                    Log.i("###BARBERID", String.valueOf(response.body().getAppointmentid()));
                }
                else
                    Log.i("###BARBERRESPONSEERRRoR","error");
            }

            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {
                Log.e("###EROR", String.valueOf(t));
                Toast.makeText(CreateAppointment.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    protected void createAppointment2(Appointment appointment){

        httpOperations();
        AppointmentAPI appointmentAPI =
                APIClient.getClient().create(AppointmentAPI.class);
        appointmentAPI.create(appointment).enqueue(new Callback<Appointment>() {
            @Override
            public void onResponse(Call<Appointment> call, Response<Appointment> response) {
                if(response.isSuccessful()){
                    loadShopSlot(response.body().getAppointmentid());
                    Log.i("###BARBERID", String.valueOf(response.body().getAppointmentid()));
                }
                else
                    Log.i("###BARBERRESPONSEERRRoR","error");
            }

            @Override
            public void onFailure(Call<Appointment> call, Throwable t) {
                Log.e("###EROR", String.valueOf(t));
                Toast.makeText(CreateAppointment.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void loadShopSlot(final Integer appointmentid){
        ShopSlotAPI shopSlotAPI =
                APIClient.getClient().create(ShopSlotAPI.class);
        shopSlotAPI.find(shopslotid).enqueue(new Callback<ShopSlot>() {
            @Override
            public void onResponse(Call<ShopSlot> call, Response<ShopSlot> response) {
                ShopSlot shopSlot = response.body();
                updateShopSlot(shopSlot, appointmentid);
            }

            @Override
            public void onFailure(Call<ShopSlot> call, Throwable t) {

            }
        });
    }

    public void updateShopSlot(ShopSlot shopSlot, Integer appointmentid){
        shopSlot.setAppointmentid(appointmentid);
        ShopSlotAPI shopSlotAPI =
                APIClient.getClient().create(ShopSlotAPI.class);
        shopSlotAPI.update(shopSlot).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.isSuccessful())
                    Log.i("UPDATED", "successful");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    private Appointment setAppointmentFormValues() {
        Appointment appointment = new Appointment();
        appointment.setShopid(shopID);
        appointment.setBarberid(barberid);
        appointment.setStarttime(time);
        appointment.setBuyableid(buyableid);
        //CAUSE I DO NOT HAVE LOGIN IN WORKING YET
        appointment.setClientid(CreateAppointment.clientID);
        return appointment;
    }



    private void loadClientID(){
        httpOperations();
        ClientAPI clientAPI = APIClient.getClient().create(ClientAPI.class);
        clientAPI.findAll().enqueue(new Callback<List<Client>>() {

            @Override
            public void onResponse(Call<List<Client>> call,
                                   Response<List<Client>> response) {
                Appointment appointment = new Appointment();
               if(response.isSuccessful()){
                   for(Client client : response.body()){
                       if(client.getUserid() == userID) {
                           appointment.setShopid(shopID);
                           appointment.setBarberid(barberid);
                           appointment.setStarttime(time);
                           appointment.setBuyableid(buyableid);
                           //CAUSE I DO NOT HAVE LOGIN IN WORKING YET
                           appointment.setClientid(client.getClientid());
                           createAppointment2(appointment);
                       }
                   }
               }
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {

            }
        });
    }

    protected void httpOperations(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
    }
}
