package com.codify.barberfinder_mob_and.Fragments.Client.Booking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.codify.barberfinder_mob_and.API.APIClient;
import com.codify.barberfinder_mob_and.API.ShopAPI;
import com.codify.barberfinder_mob_and.API.ShopTimetableAPI;
import com.codify.barberfinder_mob_and.Util.ShopAdapter;
import com.codify.barberfinder_mob_and.Entities.Shop;
import com.codify.barberfinder_mob_and.Entities.ShopTimetable;
import com.codify.barberfinder_mob_and.Fragments.Client.Booking.Timetable.WeekDayPicker;
import com.codify.barberfinder_mob_and.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectShop extends AppCompatActivity {

    private static final ArrayList<Shop> shops_al = new ArrayList<Shop>();

        public int test2 = 0;

    ShopTimetableAPI shopTimeAPI;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        shops_al.clear();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_shop);
        loadData();
    }

    public void loadData() {
        try {
            ShopAPI ShopAPI =
                    APIClient.getClient().create(ShopAPI.class);
            ShopAPI.findAll().enqueue(new Callback<List<Shop>>() {
                @Override
                public void onResponse(Call<List<Shop>> call,
                                       Response<List<Shop>> response) {
                    test2=3;
                    if(response.isSuccessful()) {
                        ArrayList<Shop> shop_al_all = new ArrayList<Shop>();
                        for (int i = 0; i < response.body().size(); i++) {
                            Shop shop = response.body().get(i);
                            shop_al_all.add(shop);
                        }
                        isItOpen_HTTP(shop_al_all);
                    }
                    printTest();
                }

                @Override
                public void onFailure(Call<List<Shop>> call,
                                      Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Toast.makeText(SelectShop.this, e.getMessage(), Toast.LENGTH_SHORT);
        }
    }

    public void isItOpen_HTTP(final ArrayList<Shop> shop_all){

            shopTimeAPI =
                    APIClient.getClient().create(ShopTimetableAPI.class);
            shopTimeAPI.findAll().enqueue(new Callback<List<ShopTimetable>>() {
                @Override
                public void onResponse(Call<List<ShopTimetable>> call,
                                       Response<List<ShopTimetable>> response) {
                    long time = getIntent()
                            .getLongExtra("Time", 0L);

                    printTest();
                    for (ShopTimetable shopTime : response.body())
                        for (Shop shop : shop_all)
                            if (shopTime.getShopid().equals(shop.getShopid()))
                                if (isItOpenAtThisTime(shopTime.getOpenhour(),
                                        shopTime.getClosinghour(), time))
                                    shops_al.add(shop);

                    ListView();
                }

                @Override
                public void onFailure(Call<List<ShopTimetable>> call,
                                      Throwable t) {
                    Log.i("###Shops_al", t.toString());
                }
            });
    }
    public void printTest(){
        Log.i("###TEST", String.valueOf(test2));
    }

    private boolean isItOpenAtThisTime(long open, long close, long requested){
        Calendar cReq = Calendar.getInstance();
        Calendar cOpen = Calendar.getInstance();
        Calendar cClose = Calendar.getInstance();

        cOpen.setTimeInMillis(open);
        cClose.setTimeInMillis(close);
        cReq.setTimeInMillis(requested);

        cClose= allTheSame(cClose);
        cReq = allTheSame(cReq);
        cOpen = allTheSame(cOpen);

        Log.i("###TIME", "Open: "+String.valueOf(cOpen.getTime())
                +"  Closed: "+String.valueOf(cClose.getTime())
                +"  Required: "+String.valueOf(cReq.getTime()));
        Log.i("###ComparedTime", "Open: "
                + String.valueOf(cReq.compareTo(cOpen))
                +"Closed: "+String.valueOf(cReq.compareTo(cClose)));


        return   cReq.compareTo(cOpen) >= 0  && cReq.compareTo(cClose) <=0;
    }

    private Calendar allTheSame(Calendar c){
        c.set(Calendar.DAY_OF_MONTH, 2);
        c.set(Calendar.MONTH, 1);
        c.set(Calendar.YEAR, 2019);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c;
    }




    private void ListView(){
        int usrID = getIntent().getIntExtra("USERID", 0);
        long time = getIntent()
                .getLongExtra("Time", 0L);
        ListAdapter shopAdapter = new ShopAdapter(this, shops_al, usrID, time);
        ListView shopListView = (ListView) findViewById(R.id.shopListView);
        shopListView.setAdapter(shopAdapter);
        Log.i("YOU ARE CLICKING", "yep you are");
        shopListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        Shop shop = (Shop) parent.getItemAtPosition(position);
                        Toast.makeText(SelectShop.this, shop.toString(),
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SelectShop.this,
                                WeekDayPicker.class);
                        intent.putExtra("SHOPID",shop.getShopid());
                        intent.putExtra("USERID", getIntent()
                                .getIntExtra("USERID", 0));

                        startActivity(intent);
                    }
                }
        );
    }
}
