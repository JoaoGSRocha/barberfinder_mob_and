package com.codify.barberfinder_mob_and.Fragments.Client.Booking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.codify.barberfinder_mob_and.Util.ClientAppointment.CalendarPicker;
import com.codify.barberfinder_mob_and.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class ClientBooking extends Fragment {

    private TextView theDate;
    private Button btnGoCalendar;
    private Button btnFindMatches;
    Intent nIntent;
    Activity a;


    public ClientBooking() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



        if(context instanceof  Activity){
            a=(Activity) context;
        }

        nIntent = new Intent(a, SelectShop.class);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Date date = getDate(data.getLongExtra("Date", 0L),
                        "dd/MM/yyyy HH:mm:ss");
                Log.i("FinalDate", String.valueOf(date));

                //This will be how we are going to pass the value to
                //the DB
                Log.i("##DateMill",
                        String.valueOf(data.getLongExtra("Date", 0L)));
                nIntent.putExtra("Time",data.getLongExtra("Date", 0L));

                theDate.setText(date.toString());
            }
        }
    }

    public static Date getDate(long milliSeconds, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        String txt = formatter.format(calendar.getTime());

        try {
            return formatter.parse(txt);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_client_booking,
                container, false);
        Integer usrID = getArguments().getInt("USERID");
        Integer currDOW = getArguments().getInt("DOW");
        Log.i("###USERID", String.valueOf(usrID));
        nIntent.putExtra("USERID", usrID);
        theDate = (TextView) rootView.findViewById(R.id.confirmationtxt);
        btnGoCalendar = (Button) rootView.findViewById(R.id.btn_calendar);
        btnFindMatches = (Button) rootView.findViewById(R.id.btnHomePage);

        btnGoCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(a, CalendarPicker.class);
                startActivityForResult(intent, 1);
            }
        });

        btnFindMatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(nIntent);
            }
        });
        return rootView;
    }
}
