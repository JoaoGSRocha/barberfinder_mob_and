package com.codify.barberfinder_mob_and.Fragments.Client.Booking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.codify.barberfinder_mob_and.R;
import com.codify.barberfinder_mob_and.Util.ClientAppointment.CalendarPicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SelectTime_Date extends AppCompatActivity {

    private TextView theDate;
    private Button btnGoCalendar;
    private Button btnFindMatches;
    Intent nIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nIntent = new Intent(SelectTime_Date.this,
                SelectShop.class);
        setContentView(R.layout.activity_select_time_date);
        theDate = (TextView) findViewById(R.id.confirmationtxt);
        btnGoCalendar = (Button) findViewById(R.id.btn_calendar);
        btnFindMatches = (Button) findViewById(R.id.btnHomePage);

        btnGoCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectTime_Date.this,
                        CalendarPicker.class);
                startActivityForResult(intent, 1);
            }
        });

        btnFindMatches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               startActivity(nIntent);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Date date = getDate(data.getLongExtra("Date", 0L),
                        "dd/MM/yyyy HH:mm:ss");
                Log.i("FinalDate", String.valueOf(date));

                //This will be how we are going to pass the value to
                //the DB
                Log.i("##DateMill",
                        String.valueOf(data.getLongExtra("Date", 0L)));
                nIntent.putExtra("Time",data.getLongExtra("Date", 0L));

                theDate.setText(date.toString());
            }
        }
    }

    public static Date getDate(long milliSeconds, String dateFormat)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        String txt = formatter.format(calendar.getTime());

        try {
            return formatter.parse(txt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
